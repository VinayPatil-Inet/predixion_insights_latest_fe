import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ChildActivationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { BnNgIdleService } from 'bn-ng-idle';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    title = 'HealthChain.io';
    constructor(public router: Router, private titleService: Title,private bnIdle: BnNgIdleService) {
        this.router.events
            .pipe(filter(event => event instanceof ChildActivationEnd))
            .subscribe((event: any) => {
                let snapshot = (event as ChildActivationEnd).snapshot;
                while (snapshot.firstChild !== null) {
                    snapshot = snapshot.firstChild;
                }
                this.titleService.setTitle(snapshot.data.title || 'HealthChain.io');
                this.titleService.setTitle('HealthChain.io');
            });
            this.bnIdle.startWatching(10000).subscribe((res: any) => {
                if(res) {
                 //  alert("session expired");
                //  this.router.navigate(['dashboard']);
                   this.router.navigate(['/auth/login']);
                }
              })
    }
}
