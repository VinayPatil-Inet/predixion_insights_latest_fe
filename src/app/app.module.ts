import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {DataTablesModule} from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BnNgIdleService } from 'bn-ng-idle'; 

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

 
import { ChartsModule } from 'ng2-charts';


// import { FileuploadComponent } from './modules/fileupload/fileupload.component';
// import { FileuploadComponent } from '../modules/fileupload/fileupload.component';


@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule, DataTablesModule,BsDatepickerModule.forRoot(), NgMultiSelectDropDownModule.forRoot(), NgxSliderModule, ChartsModule],
    providers: [BnNgIdleService],
    bootstrap: [AppComponent],
})
export class AppModule {}
