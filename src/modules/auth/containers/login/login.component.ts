import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginModel } from '../login/login.model';
import { DataManagerService } from '../service/data-manager.service';
import { environment } from '../../../../environments/environment';
const APIEndpoint = environment.APIEndpoint;

@Component({
    selector: 'sb-login',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './login.component.html',
    styleUrls: ['login.component.scss'],
})
export class LoginComponent implements OnInit {
    user: LoginModel;
    loginForm?: FormGroup;
    submitted: boolean = false;
    loginUrl: string = '';
    errorMessage: boolean = false; 
    public serverEndPoint = APIEndpoint;
 
    // Constructor
    constructor(private formBuilder: FormBuilder, private router: Router, private dataManager: DataManagerService) {
        this.user = new LoginModel();
    }

    ngOnInit(): void {
        console.log("Loading login Component...");
        this.loginUrl = this.serverEndPoint+"/rest/private/dma/v1/user/signIn";
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")]],
            apassword: ['', [Validators.required, Validators.minLength(8)]]
        });
    }

    get f() { return this.loginForm?.controls; }

    onLogin() {
        console.log("calling login function...");
        this.submitted = true;
        if (this.loginForm?.invalid) {
            return;
        }
        // Login API Call
        this.dataManager.postRequest(this.loginUrl, this.user)
            .subscribe((response: any) => {
                console.log("Login Responce", response);
                // sessionStorage.setItem('user', response.user);
                sessionStorage.setItem('user', JSON.stringify(response.user));
                sessionStorage.setItem('token', response.token);
                this.router.navigate(['dashboard/dashboardnew']);
            }, (error: any) => {
                this.errorMessage = true;
            });
        // End
    }

    logOut() {
        sessionStorage.clear();
        this.router.navigate(['login']);
    }

    focusFunction() {
        this.errorMessage = false;
    }
}
