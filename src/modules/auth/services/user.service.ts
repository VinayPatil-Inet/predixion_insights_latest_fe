import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

import { User } from '../models';

const userSubject: ReplaySubject<User> = new ReplaySubject(1);

@Injectable()
export class UserService {

    // var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
    // this.userId = currentUser.id
    
    constructor() {
        var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
        // console.log(currentUser.firstname , "user name....");
        this.user = {
            id: currentUser.id,
            firstName: currentUser.firstname,
            lastName: currentUser.lastname,
            email: currentUser.email,
            logo: currentUser.logo,
        };
    }

    set user(user: User) {
        userSubject.next(user);
    }

    get user$(): Observable<User> {
        return userSubject.asObservable();
    }
}
