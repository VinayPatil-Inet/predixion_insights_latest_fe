import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '@modules/auth/services';
import { SideNavItems, SideNavSection } from '@modules/navigation/models';
import { NavigationService } from '@modules/navigation/services';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'sb-side-nav',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './side-nav.component.html',
    styleUrls: ['side-nav.component.scss'],
})
export class SideNavComponent implements OnInit, OnDestroy {
    @Input() sidenavStyle!: string;
    @Input() sideNavItems!: SideNavItems;
    @Input() sideNavSections!: SideNavSection[];

    subscription: Subscription = new Subscription();
    routeDataSubscription!: Subscription;
    retention_arrow: string = "";
    acquisition_arrow: string = "";

    constructor(public navigationService: NavigationService, public userService: UserService, public activatedRoute: ActivatedRoute, public router: Router) {
        this.activatedRoute.params.subscribe(params => {
            console.log(this.router.url)

            var dataArr = this.router.url.split('/');
            var url = dataArr[0];
            var name = dataArr[2];
            //var dashboard = dataArr[1];

            console.log("name1 = " + name)
            if ((name == "retention-dashboard") || (name == "rerentionfileupload") || (name == "retention-int-view") || (name == "memberretention") || (name == "memberdetails")) {
                console.log("name2 = " + name)
                this.acquisition_status = false;
                this.retention_status = true;
                this.retention_arrow = "['fas', 'angle-down']";
                this.acquisition_arrow = "['fas', 'angle-right']";

            }

            if ((name == "dashboardnew") || (name == "file") || (name == "file-int-view") || (name == "predictions") || (name == "reports") ) {
                console.log("name2 = " + name)
                this.acquisition_status = true;
                this.retention_status = false;
                this.retention_arrow = '["fas", "angle-right"]';
                this.acquisition_arrow = '["fas", "angle-down"]';

            }




        })

    }

    ngOnInit() { }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    acquisition_status: boolean = false;
    retention_status: boolean = false;
    //retention_arrows: boolean = false;


    Open_Acquisition() {

        this.acquisition_status = !this.acquisition_status;

    }

    Open_Retention() {

        this.retention_status = !this.retention_status;
        //this.retention_arrows = !this.retention_arrow;

    }


}
