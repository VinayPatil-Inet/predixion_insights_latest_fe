import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/index';

import {ResDataModal} from '../../dashboard/containers/view-grid/resDataModal';

@Injectable({
  providedIn: 'root'
})
export class DataManagerService {

  apiUrl = 'https://reqres.in/api/users?page=1';

  httpHeaders?: HttpHeaders;
  constructor(private httpClient: HttpClient) { }

  setHeader() {
    this.httpHeaders = new HttpHeaders().set('Accept', 'application/json');
    this.httpHeaders = this.httpHeaders.append('Content-Type', 'text'); 
    this.httpHeaders = this.httpHeaders.append('Access-Control-Allow-Origin', 'http://164.52.194.25:8017'); 
    this.httpHeaders = this.httpHeaders.append('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT'); 
    this.httpHeaders = this.httpHeaders.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); 
    this.httpHeaders = this.httpHeaders.append('Access-Control-Allow-Credentials', 'true'); 
  }

  getRequest(url: string): Observable<any> {
    return this.httpClient.get(url, { headers: this.httpHeaders });
  }

  postRequest(url: string, payload: any): Observable<any> {
    // console.log('url', url); 
    return this.httpClient.post(url, payload, { headers: this.httpHeaders });
  }

  getData(): Observable<ResDataModal> {
    return this.httpClient.get<ResDataModal>(this.apiUrl);
  }

}
