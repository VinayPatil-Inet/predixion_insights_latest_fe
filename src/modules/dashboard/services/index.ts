import { DashboardService } from './dashboard.service';
import { DataManagerService } from './data-manager.service';

export const services = [DashboardService , DataManagerService];

export * from './dashboard.service';
export * from './data-manager.service';
