//added for select box
import { FormBuilder, FormGroup } from '@angular/forms';
import { AfterViewInit, EventEmitter, ElementRef, Component, ChangeDetectionStrategy, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { DataManagerService } from '../../services/data-manager.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { NgbDatepickerNavigateEvent } from '@ng-bootstrap/ng-bootstrap';
import { Options } from '@angular-slider/ngx-slider';
// import { ChartDataSets, ChartOptions } from 'chart.js';
import { Chart } from 'chart.js';

const APIEndpoint = environment.APIEndpoint;
class Person {
  id?: number;
  firstName?: string;
  lastName?: string;
}
class DataTablesResponse {
  data?: any[];
  draw?: number;
  recordsFiltered?: number;
  recordsTotal?: number;
}
@Component({
  selector: 'sb-memberdetails',
  templateUrl: './memberdetails.component.html',
  styleUrls: ['./memberdetails.component.scss']
})
export class MemberdetailsComponent implements AfterViewInit, OnDestroy, OnInit {

  @ViewChild(DataTableDirective, { static: false })
  dtElement?: DataTableDirective;
  errorMessage: boolean = false;
  successMessage: boolean = false;
  errorMessageText?: string;
  successMessageText?: string;
  waitMessage: boolean = false;
  waitMessageText?: string;
  isSubmitBtnDisabled: boolean = false;
  paginationCount?: number;
  dtTrigger: Subject<any> = new Subject();
  public serverEndPoint = APIEndpoint;
  getIntTableUrl: string = "";
  totalPushToSalesForce: string = "";
  userId: string = "";
  minScore: string = '0t25';
  minValue: number = 16;
  maxValue: number = 100;
  options: Options = {
    floor: 1,
    ceil: 120
  };
  currentYear: number = new Date().getFullYear();
  fileObject: any = {
    id:88,
    fromDate: "2017-08-17",
    toDate: "2017-12-27",
    // fileMonth: new Date().getMonth() + 1,
    // fileYear: new Date().getFullYear(),
    state: [],
    city: [],
    income: [],
    zipcode: [],
    age: { min: 16, max: 120 },
    integratedLead: [],
    isChecked: false,
    isCheckedLeadsAll: false,
    assignee: [],
    disenrollment: [],
    changeDisenrollment: [],
    cummunicationReference: [],
    outreachStage: [],
    memberPrioritization: [],
    score: [{ id: '75t100', value: 'Extremely High', ischecked: false }, { id: '50t75', value: 'High', ischecked: false }, { id: '25t50', value: 'Medium', ischecked: false }, { id: '0t25', value: 'Below Medium', ischecked: false }],
  };
  // Dropdown 
  myForm?: FormGroup;
  disabled = false;
  ShowFilter = true;
  limitSelection = false;
  selectedItems?: any[];
  dropdownSettings: any = {};
  getMemberDetailsUrl: string = "";
  pushToSalesForceUrl: string = "";
  checkboxSelectAllUrl: string = "";
  updatePrediction: string = "";
  predictionUrl: string = "";
  updatePredictionScore: string = "";
  totalLeadCount?: string = '0';
  retentionlistTable?: any[];
  stats?: any[];
  zipcodes?: any[];
  income?: any[];

  totalMember?: number = 50000;

  totalMemberData?: any[];

  resMemberDetails?: any[];

  dtOptions: DataTables.Settings = {};
  persons?: Person[];
  public constructor(private router: Router, private dataManager: DataManagerService, private activatedRoute: ActivatedRoute, private http: HttpClient, private fb: FormBuilder) {
    this.activatedRoute.params.subscribe(params => {
      console.log("URL params ", params);

      if (params.filter != undefined) {
        this.fileObject.id = params.filter; 
        console.log("fileObject ", this.fileObject);
      }
     
    })
  }

  public ngOnInit(): void {
    // this.totalMemberData =  [10000,30162,26263,18394,28682,50000];
    console.log("Calling init function...");
    var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
    this.userId = currentUser.id
    //API urls
    this.getMemberDetailsUrl = this.serverEndPoint + "/rest/private/dma/v1/getmemberdetails";

    if (sessionStorage.getItem("token") === null) {
      this.router.navigate(['auth/login']);
    }
    this.getMemberDetails();
  }

  getMemberDetails() {
    console.log("inside getMemberDetails", this.fileObject);
    this.dataManager.postRequest(this.getMemberDetailsUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "getMemberDetailsUrl response");
        this.resMemberDetails = response;
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log(error, "getMemberDetailsUrl error");
        this.retentionlistTable = [];
      });
  }

  pushToSalesForce() {
    console.log("Inside Update Prediction function..");
    this.errorMessage = false;
    this.successMessage = false;
    this.waitMessage = true;
    this.waitMessageText = "Please Wait.. Leads Are Exporting Into Salesforce..!";
    this.isSubmitBtnDisabled = true;
    this.fileObject.userId = this.userId;
    console.log("Prediction Payload", this.fileObject);

    setTimeout(() => {
      this.waitMessage = false;
      this.errorMessage = false;
      this.successMessage = true;
      this.successMessageText = "Leads Are Exported Successfully Into Salesforce..!";
      this.isSubmitBtnDisabled = false;
  }, 5000);
  
  }


  dashboaredGo() {
    this.router.navigate(['dashboard/retention-dashboard']);
  }

  goBack() {
    this.router.navigate(['dashboard/retention-int-view']);
  }

  ngAfterViewInit(): void {
    // this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}

