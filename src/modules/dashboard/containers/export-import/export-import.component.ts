import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { DataManagerService } from '../../services/data-manager.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: 'sb-export-import',
  templateUrl: './export-import.component.html',
  styleUrls: ['./export-import.component.scss']
})
export class ExportImportComponent implements AfterViewInit, OnDestroy, OnInit {

  errorMessage: boolean = false;
  successMessage: boolean = false;
  errorMessageText?: string;
  successMessageText?: string;
  waitMessage: boolean = false;
  waitMessageText?: string;
  isSubmitBtnDisabled: boolean= false;
  fileObject: any = {
    fileMonth: '',
    fileYear: "",
    userId: "",
    fileTypeId: "",
    fileType: "",
    tableName: "",
    fileName: ""
  };
  SalesforceAllList?: [];
  getSalesforceListUrl: string = "";

  public serverEndPoint = APIEndpoint;
  testUrl: string = "";
  importUrl: string = "";
  exportUrl: string = "";
  importExportObject: any = {
    userId: "",
  };
  dataTablesParameters:any={
    dateObject:""
  };
  userId: any;
  getPushedDownloadUrl: string="";
  getPulledDownloadUrl: string="";
  public constructor(private router: Router, private dataManager: DataManagerService, private activatedRoute: ActivatedRoute, private http: HttpClient) {
    this.activatedRoute.params.subscribe(params => {

    })
  }

  public ngOnInit(): void {
 if (sessionStorage.getItem("token") === null) {
      this.router.navigate(['auth/login']);
      //...
    }
    console.log("Calling init function...");
    this.testUrl = this.serverEndPoint + "/rest/private/app/v1/test";
    this.importUrl = this.serverEndPoint + "/rest/private/app/v1/import";
    this.exportUrl = this.serverEndPoint + "/rest/private/app/v1/ExportCsv";
    // this.getSalesforceListUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/type";
    this.getSalesforceListUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/SalesforceList";
    this.getPushedDownloadUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/SalesforcePushedDownload";
    this.getPulledDownloadUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/SalesforcePulledDownload";
    
    var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
    this.userId = currentUser.id
    const that = this;
    this.getSalesforceList();
  }

  getSalesforceList() {
    console.log("inside getSalesforceList");
    this.fileObject.userId = this.userId;
    this.fileObject.fileMonth = '10';
    this.fileObject.fileYear = "2021";
    console.log("inside getSalesforceList", this.fileObject);
    this.dataManager.postRequest(this.getSalesforceListUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "getSalesforceList List Response1111111111......");
         this.SalesforceAllList = response;
        
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log("getSalesforceList == ",error);
      });
  }

  DownloadSalesforcePulledFile(month: any,year: any) {
    console.log("inside DownloadSalesforcePulledFile",month,year);
    this.fileObject.userId = this.userId;
    this.fileObject.fileMonth = month;
    this.fileObject.fileYear = year;
   
    console.log("inside DownloadSalesforcePulledFile", this.fileObject);
    this.dataManager.postRequest(this.getPulledDownloadUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "DownloadSalesforcePulledFile List Response1111111111......");
         this.SalesforceAllList = response;
         var downloadfilename = 'HealthChain-Downloaded-from-SF.xlsx';
         if (response.length > 0) {
          const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
          const EXCEL_EXTENSION = '.xlsx';
          const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(response);
          console.log('worksheet', worksheet);
          const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
          const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
          var fileName = downloadfilename;
          const data: Blob = new Blob([excelBuffer], {
            type: EXCEL_TYPE
          });
          FileSaver.saveAs(data, fileName);
          this.getSalesforceList();
        } 
        
        else {
          
          alert('No data for this month and year');
          this.getSalesforceList();
        }
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log("getSalesforceList == ",error);
      });
  }
  DownloadSalesforcePushedFile(month: any,year: any) {
    console.log("inside DownloadSalesforcePushedFile",month,year);
    this.fileObject.userId = this.userId;
    this.fileObject.fileMonth = month;
    this.fileObject.fileYear = year;
   
    console.log("inside DownloadSalesforcePushedFile", this.fileObject);
    this.dataManager.postRequest(this.getPushedDownloadUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "DownloadSalesforcePushedFile List Response1111111111......");
         this.SalesforceAllList = response;
         var downloadfilename = 'HealthChain-Uploaded-to-SF.xlsx';
         if (response.length > 0) {
          const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
          const EXCEL_EXTENSION = '.xlsx';
          const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(response);
          console.log('worksheet', worksheet);
          const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
          const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
          var fileName = downloadfilename;
          const data: Blob = new Blob([excelBuffer], {
            type: EXCEL_TYPE
          });
          FileSaver.saveAs(data, fileName);
          this.getSalesforceList();
        } 
        
        else {
          
          alert('No data for this month and year')
          this.getSalesforceList();
        }
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log("getSalesforceList == ",error);
      });
  }

  import() {
    console.log("hi Vijay.. Import Function called...");
    console.log("url..", this.importUrl);
    this.errorMessage = false;
    this.successMessage = false;
    this.waitMessage = true;
    this.waitMessageText = "Please Wait... Leads Data Importing From The Salesforce..!";
    this.isSubmitBtnDisabled = true;
    this.importExportObject.userId = "1";
    this.dataManager.postRequest(this.importUrl, this.importExportObject)
      .subscribe((response: any) => {
        console.log(response, "Import responce");
        this.ngOnInit();
        this.waitMessage = false;
        this.errorMessage = false;
        this.successMessage = true; 
        this.successMessageText = "Leads Data Imported Successfully From The Salesforce..!";
        this.isSubmitBtnDisabled = false;
        this.router.navigate(['dashboard/export-import']);
      }, (error: any) => {
        this.successMessage = false;
        this.waitMessage = false;
        this.errorMessage = true;
        this.errorMessageText = "Leads Data Import Failed..!";
        this.isSubmitBtnDisabled = false;
      });
  }

  export() {
    console.log("hi Vijay.. Export Function called...");
    this.importExportObject.userId = "1";
    this.errorMessage = false;
    this.successMessage = false;
    this.waitMessage = true;
    this.waitMessageText = "Please Wait.. Leads Data Exporting Into Salesforce..!";
    this.isSubmitBtnDisabled = true;
    this.dataManager.postRequest(this.exportUrl, this.importExportObject)
      .subscribe((response: any) => {
        console.log(response, "Export responce");
        this.ngOnInit();
        this.errorMessage = false;
        this.waitMessage = false;
        this.successMessage = true; 
        this.successMessageText = "Leads Data Exported Successfully Into Salesforce..!";
        this.isSubmitBtnDisabled = false;
        // this.router.navigate(['file']);
      }, (error: any) => {
        this.successMessage = false;
        this.waitMessage = false;
        this.errorMessage = true;
        this.errorMessageText = "Leads Data Export Failed..!";
        this.isSubmitBtnDisabled = false;
      });
  }

  test() {
    console.log("hi Vijay.. Test Function called...");
    console.log("url..", this.testUrl);
    this.importExportObject.userId = "1";
    this.errorMessage = false;
    this.successMessage = false;
    this.dataManager.postRequest(this.testUrl, this.importExportObject)
      .subscribe((response: any) => {
        console.log(response, "Test responce");
        this.ngOnInit();
        // this.router.navigate(['dashboard/export-import']);
        this.successMessage = true;
        this.errorMessage = false;
        this.successMessageText = "Successfully Tested..";
      }, (error: any) => {
        this.successMessage = false;
        this.errorMessage = true;
        this.errorMessageText = error.error.message
      });
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event 
  }
  isShown: boolean = true ;
  isHide: boolean = false ;
  public isChecked = false;
  checked(): void {
    
    this.isShown = ! this.isShown;
      this.isHide = ! this.isHide;
      /*
    if(this.isChecked == true)
    {
      this.isShown = true ;
      this.isHide = false ;
      this.isShown = ! this.isShown;
      this.isHide = ! this.isHide;
    }
    else
    {
      console.log("ddddd");
      this.isShown = false ;
      this.isHide = true ;
      this.isShown = ! this.isShown;
      this.isHide = ! this.isHide;
    }
    */
  }

  
 
}
