import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionDashboardComponent } from './retention-dashboard.component';

describe('MemberretentionComponent', () => {
  let component: RetentionDashboardComponent;
  let fixture: ComponentFixture<RetentionDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetentionDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
