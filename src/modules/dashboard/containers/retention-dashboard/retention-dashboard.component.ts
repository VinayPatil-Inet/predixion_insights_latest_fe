//added for select box
import { FormBuilder, FormGroup } from '@angular/forms';
import { AfterViewInit, EventEmitter, ElementRef, Component, ChangeDetectionStrategy, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { DataManagerService } from '../../services/data-manager.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { NgbDatepickerNavigateEvent } from '@ng-bootstrap/ng-bootstrap';
import { Options } from '@angular-slider/ngx-slider';
// import { ChartDataSets, ChartOptions } from 'chart.js';
import { Chart } from 'chart.js';

const APIEndpoint = environment.APIEndpoint;
class Person {
  id?: number;
  firstName?: string;
  lastName?: string;
}
class DataTablesResponse {
  data?: any[];
  draw?: number;
  recordsFiltered?: number;
  recordsTotal?: number;
}
@Component({
  selector: 'sb-retention-dashboard',
  templateUrl: './retention-dashboard.component.html',
  styleUrls: ['./retention-dashboard.component.scss']
})
export class RetentionDashboardComponent implements AfterViewInit, OnDestroy, OnInit {
  /* chart code */
  @ViewChild('myPieChart') myPieChart!: ElementRef<HTMLCanvasElement>;
  @ViewChild('newMemberChart') myAreaChart!: ElementRef<HTMLCanvasElement>;
  @ViewChild('TotalMemberChart') TotalMemberChart!: ElementRef<HTMLCanvasElement>;
  @ViewChild('BarChart') BarChart!: ElementRef<HTMLCanvasElement>;
  chart!: Chart;
  /* end chart code */
  @ViewChild(DataTableDirective, { static: false })
  dtElement?: DataTableDirective;
  errorMessage: boolean = false;
  successMessage: boolean = false;
  errorMessageText?: string;
  successMessageText?: string;
  waitMessage: boolean = false;
  waitMessageText?: string;
  isSubmitBtnDisabled: boolean = false;
  paginationCount?: number;
  dtTrigger: Subject<any> = new Subject();
  public serverEndPoint = APIEndpoint;
  getIntTableUrl: string = "";
  totalPushToSalesForce: string = "";
  userId: string = "";
  minScore: string = '0t25';
  minValue: number = 16;
  maxValue: number = 100;
  options: Options = {
    floor: 1,
    ceil: 120
  };
  currentYear: number = new Date().getFullYear();
  fileObject: any = {
    fromDate: "2021-08-01",
    toDate: "2022-03-08",
    // fileMonth: new Date().getMonth() + 1,
    // fileYear: new Date().getFullYear(),
    state: [],
    city: [],
    income: [],
    zipcode: [],
    age: { min: 16, max: 120 },
    integratedLead: [],
    isChecked: false,
    isCheckedLeadsAll: false,
    assignee: [],
    disenrollment: [],
    changeDisenrollment: [],
    cummunicationReference: [],
    outreachStage: [],
    memberPrioritization: [],
    score: [{ id: '75t100', value: 'Extremely High', ischecked: false }, { id: '50t75', value: 'High', ischecked: false }, { id: '25t50', value: 'Medium', ischecked: false }, { id: '0t25', value: 'Below Medium', ischecked: false }],
  };
  // Dropdown 
  myForm?: FormGroup;
  disabled = false;
  ShowFilter = true;
  limitSelection = false;
  selectedItems?: any[];
  dropdownSettings: any = {};
  getRetentionListUrl: string = "";
  pushToSalesForceUrl: string = "";
  checkboxSelectAllUrl: string = "";
  updatePrediction: string = "";
  predictionUrl: string = "";
  updatePredictionScore: string = "";
  totalLeadCount?: string = '0';
  retentionlistTable?: any[];
  stats?: any[];
  zipcodes?: any[];
  income?: any[];

  totalMember?: number = 500874;

  totalMemberData?: any[];

  dtOptions: DataTables.Settings = {};
  persons?: Person[];
  public constructor(private router: Router, private dataManager: DataManagerService, private activatedRoute: ActivatedRoute, private http: HttpClient, private fb: FormBuilder,private elementRef: ElementRef) {
    this.activatedRoute.params.subscribe(params => {
      if (params.month != undefined) {
        this.fileObject.fileMonth = params.month;
        this.fileObject.fileYear = params.year;
        console.log("URL Month ", this.fileObject);
      }
    })
  }

  public ngOnInit(): void {
    // this.totalMemberData =  [10000,30162,26263,18394,28682,50000];
    this.totalMember =500874;
    this.totalMemberData =  [100000,301620,262630,183940,286820,500874];
    console.log("Calling init function...");
    var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
    this.userId = currentUser.id
    //API urls
    this.getRetentionListUrl = this.serverEndPoint + "/rest/private/dma/v1/memberretention";

    if (sessionStorage.getItem("token") === null) {
      this.router.navigate(['auth/login']);
    }
    this.getRetentionList();

    this.test1234();
  }
  status1: boolean = true;
  test1234()
  {
    
    console.log("Hello ");
    this.status1 = true;
    console.log("Hello122 ");
  }
  getRetentionList() {
    console.log("inside getRetentionList", this.fileObject);
    this.dataManager.postRequest(this.getRetentionListUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "getRetentionListUrl response");
        this.retentionlistTable = response;
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log(error, "getRetentionListUrl error");
        this.retentionlistTable = [];
      });
  }

  formDate(item: any) {
    console.log('fromDate change ,......', item.target.value);
    this.fileObject.fromDate = item.target.value;
    this.ngOnInit();
  }

  toDate(item: any) {
    console.log('toDate change ,......', item.target.value);
    this.fileObject.toDate = item.target.value;
    this.ngOnInit();
  }


  changeCity(item: any) {
    if (item.target.value) {
      console.log('city change ,......', item.target.value);
      this.fileObject.city = [];
      this.fileObject.city.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.city = [];
      this.ngOnInit();
    }
  }

  changezipcode(item: any) {
    if (item.target.value) {
      console.log('city change ,......', item.target.value);
      this.fileObject.zipcode = [];
      this.fileObject.zipcode.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.zipcode = [];
      this.ngOnInit();
    }
  }

  changeState(item: any) {
    if (item.target.value) {
      console.log('State change ,......', item.target.value);
      this.fileObject.state = [];
      this.fileObject.state.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.state = [];
      this.ngOnInit();
    }
  }

  changeIncome(item: any) {
    if (item.target.value) {
      console.log('changeIncome change ,......', item.target.value);
      this.fileObject.income = [];
      this.fileObject.income.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.income = [];
      this.ngOnInit();
    }
  }


  changeAssignee(item: any) {
    if (item.target.value) {
      console.log('changeAssignee change ,......', item.target.value);
      this.fileObject.assignee = [];
      this.fileObject.assignee.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.assignee = [];
      this.ngOnInit();
    }
  }

  changeDisenrollment(item: any) {
    if (item.target.value) {
      console.log('disenrollment change ,......', item.target.value);
      this.fileObject.disenrollment = [];
      this.fileObject.disenrollment.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.disenrollment = [];
      this.ngOnInit();
    }
  }
  changeCummunicationReference(item: any) {
    if (item.target.value) {
      console.log('cummunicationReference change ,......', item.target.value);
      this.fileObject.cummunicationReference = [];
      this.fileObject.cummunicationReference.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.cummunicationReference = [];
      this.ngOnInit();
    }
  }
  changeOutreachStage(item: any) {
    if (item.target.value) {
      console.log('outreachStage change ,......', item.target.value);
      this.fileObject.outreachStage = [];
      this.fileObject.outreachStage.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.outreachStage = [];
      this.ngOnInit();
    }
  }
  changeMemberPrioritization(item: any) {
    if (item.target.value) {
      console.log('memberPrioritization change ,......', item.target.value);
      this.fileObject.memberPrioritization = [];
      this.fileObject.memberPrioritization.push(item.target.value);
      this.ngOnInit();
    } else {
      this.fileObject.memberPrioritization = [];
      this.ngOnInit();
    }
  }

  changeMember(item: any) {
    if (item.target.value == 'Medicare') {
      console.log('changeMember change ,......', item.target.value);
      this.totalMember =300874;
      this.totalMemberData =  [50000,201620,102630,183940,206820,300874];
      this.ngAfterViewInit();
    }
    else if (item.target.value == '') {
      console.log('changeMember change ,......', item.target.value);
      this.totalMember =500874;
      this.totalMemberData = [100000,301620,262630,183940,286820,500874];
      this.ngAfterViewInit();
    }
    else {
      console.log('changeMember change ,......', item.target.value);
      this.totalMember =200000;
      this.totalMemberData =  [50000,152630,101620,90940,186820,200000];
      this.ngAfterViewInit();
      // this.ngOnInit();
    }
  }


  pushToSalesForce() {
    console.log("Inside Update Prediction function..");
    this.errorMessage = false;
    this.successMessage = false;
    this.waitMessage = true;
    this.waitMessageText = "Please Wait.. Leads Are Exporting Into Salesforce..!";
    this.isSubmitBtnDisabled = true;
    this.fileObject.userId = this.userId;
    console.log("Prediction Payload", this.fileObject);

    setTimeout(() => {
      this.waitMessage = false;
      this.errorMessage = false;
      this.successMessage = true;
      this.successMessageText = "Leads Are Exported Successfully Into Salesforce..!";
      this.isSubmitBtnDisabled = false;
  }, 5000);
  
  }

  ngAfterViewInit(): void {
    // this.dtTrigger.next();
    const dom: HTMLElement = this.elementRef.nativeElement;
    const elements = dom.querySelectorAll('.retention_submenu')
    console.log("test")
    elements.forEach((domElement)=>{
      domElement.classList.add('ret_open');

      console.log("ret_open")
    })
    console.log("test1111")
    this.chart = new Chart(this.myPieChart.nativeElement, {
      type: 'pie',
      data: {

        datasets: [
          {
            data: [40, 30, 20],
            backgroundColor: ['#004A75', '#FFD66B', '#FF8F6B'],
          },
        ],
        labels: ['Low Risk', 'Medium Risk', 'High Risk'],
      },
      
    options: {
      legend: {
        display: false
      }
    }
      
    });

    this.chart = new Chart(this.myAreaChart.nativeElement, {
      type: 'line',
      data: {
        labels: [
          'Oct 21',
          'Nov 21',
          'Dec 21',
          'Jan 22',
          'Feb 22',
          'Mar 22'
        ],
        datasets: [
          {
            label: 'Total New Members',
            lineTension: 0.3,
            backgroundColor: 'rgb(241 96 49 / 28%)',
            borderColor: '#f16031',
            pointRadius: 5,
            pointBackgroundColor: '#f16031',
            pointBorderColor: 'rgba(255,255,255,0.8)',
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(2,117,216,1)',
            pointHitRadius: 50,
            pointBorderWidth: 2,
            data: [
              28,
              80,
              60,
              50,
              80,
              100
            ],
          },
        ],
      },
      options: {
        scales: {
          xAxes: [
            {
              
              time: {
                unit: 'day',
              },
              gridLines: {
                display: false,
              },
              ticks: {
                maxTicksLimit: 7,
              },
            },
          ],
          yAxes: [
            {
              
              ticks: {
                min: 0,
                max: 100,
                maxTicksLimit: 10,
              },
              gridLines: {
                color: 'rgba(0, 0, 0, .125)',
              },
            },
          ],
        },
        legend: {
          display: false,
        },
      },
    });


    this.chart = new Chart(this.BarChart.nativeElement, {
      type: 'bar',
      data: {
        labels: ['02/2021', '03/2021', '04/2021', '05/2021', '06/2021'],
        datasets: [
          {
            label: 'Actual',
            backgroundColor: 'rgba(210, 214, 222, 1)',
            borderColor: 'rgba(2,117,216,1)',
            data: [50, 44, 18, 48, 58],
          },

          {
            label: 'Prediction',
            backgroundColor: 'rgba(60,141,188,0.9)',
            borderColor: 'rgba(2,117,216,1)',
            data: [55, 44, 12, 38, 58],
          },
        ],
      },
      options: {
        scales: {
          xAxes: [
            {
              time: {
                unit: 'month',
              },
              gridLines: {
                display: false,
              },
              ticks: {
                maxTicksLimit: 6,
              },
            },
          ],
          yAxes: [
            {
              ticks: {
                min: 0,
                max: 100,
                maxTicksLimit: 10,
              },
              gridLines: {
                display: true,
              },
            },
          ],
        },
        legend: {
          display: false,
        },
      },
    });

    this.chart = new Chart(this.TotalMemberChart.nativeElement, {
      type: 'line',
      data: {
        labels: [
          'Oct 21',
          'Nov 21',
          'Dec 21',
          'Jan 22',
          'Feb 22',
          'Mar 22',
        ],
        datasets: [
          {
            label: 'Total Members',
            lineTension: 0.3,
            backgroundColor: 'rgba(2,117,216,0.2)',
            borderColor: 'rgba(2,117,216,1)',
            pointRadius: 5,
            pointBackgroundColor: 'rgba(2,117,216,1)',
            pointBorderColor: 'rgba(255,255,255,0.8)',
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(2,117,216,1)',
            pointHitRadius: 50,
            pointBorderWidth: 2,
            data:  this.totalMemberData,
          },
        ],
      },
      options: {
        scales: {
          xAxes: [
            {
             
              time: {
                unit: 'day',
              },
              gridLines: {
                display: false,
              },
              ticks: {
                maxTicksLimit: 7,
              },
            },
          ],
          yAxes: [
            {
              
              ticks: {
                min: 0,
                max: this.totalMember,
                maxTicksLimit: 10,
              },
              gridLines: {
                color: 'rgba(0, 0, 0, .125)',
              },
            },
          ],
        },
        legend: {
          display: false,
        },
      },
    });

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}

