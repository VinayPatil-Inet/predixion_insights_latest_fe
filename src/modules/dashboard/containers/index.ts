import { DashboardComponent } from './dashboard/dashboard.component';
import { LightComponent } from './light/light.component';
import { StaticComponent } from './static/static.component';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { ViewGridComponent } from './view-grid/view-grid.component';
import { PredictionsComponent } from './predictions/predictions.component';

import { ExportImportComponent } from './export-import/export-import.component';
import { DashboardnewComponent } from './dashboardnew/dashboardnew.component';
import { ReportsComponent } from './reports/reports.component';
import { ResDataModal } from './view-grid/resDataModal';
import { MemberdetailsComponent } from './memberdetails/memberdetails.component';
import { MemberretentionComponent } from './memberretention/memberretention.component';

import { RerentionFileuploadComponent } from './rerentionfileupload/rerentionfileupload.component';
import { RetentionIntViewComponent } from './retention-int-view/retention-int-view.component';

import { RetentionDashboardComponent } from './retention-dashboard/retention-dashboard.component';

export const containers = [DashboardComponent, DashboardnewComponent, ReportsComponent, StaticComponent, LightComponent, FileuploadComponent, ViewGridComponent,PredictionsComponent, ExportImportComponent,MemberretentionComponent, RerentionFileuploadComponent,RetentionIntViewComponent,RetentionDashboardComponent, MemberdetailsComponent];

export * from './dashboard/dashboard.component';
export * from './dashboardnew/dashboardnew.component';
export * from './reports/reports.component';
export * from './static/static.component';
export * from './light/light.component';
export * from './fileupload/fileupload.component';
export * from './view-grid/view-grid.component';
export * from './predictions/predictions.component';
export * from './export-import/export-import.component';
export * from './memberdetails/memberdetails.component';
export * from './memberretention/memberretention.component';
export * from './view-grid/resDataModal';
export * from './rerentionfileupload/rerentionfileupload.component';
export * from './retention-int-view/retention-int-view.component';
export * from './retention-dashboard/retention-dashboard.component';




