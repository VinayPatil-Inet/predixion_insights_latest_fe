import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberretentionComponent } from './memberretention.component';

describe('MemberretentionComponent', () => {
  let component: MemberretentionComponent;
  let fixture: ComponentFixture<MemberretentionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberretentionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberretentionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
