import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RerentionFileuploadComponent } from './rerentionfileupload.component';


describe('FileuploadComponent', () => {
  let component: RerentionFileuploadComponent;
  let fixture: ComponentFixture<RerentionFileuploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RerentionFileuploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RerentionFileuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


