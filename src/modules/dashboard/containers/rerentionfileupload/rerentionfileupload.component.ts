import { Component, OnInit } from '@angular/core';
import { DataManagerService } from '../../services/data-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';
import { environment } from '../../../../environments/environment';
import bsCustomFileInput from 'bs-custom-file-input';
import { NgbDatepickerNavigateEvent } from '@ng-bootstrap/ng-bootstrap';
//import { time } from 'console';
//import { DatepickerModule } from 'angular-mat-datepicker'




const APIEndpoint = environment.APIEndpoint;
@Component({
  selector: 'sb-rerentionfileupload',
  templateUrl: './rerentionfileupload.component.html',
  styleUrls: ['./rerentionfileupload.component.scss']
})
export class RerentionFileuploadComponent implements OnInit {

  errorMessage: boolean = false;
  successMessage: boolean = false;
  errorMessageText?: string;
  successMessageText?: string;
  waitMessage: boolean = false;
  waitMessageText?: string;

  public serverEndPoint = APIEndpoint;
  hasBaseDropZoneOver?: boolean;
  hasAnotherDropZoneOver?: boolean;
  response?: string;
  uploadUrl: string = "";
  importUrl: string = "";
  uploadUrl2: string = "";
  saveUrl: string = "";
  getFileTypesUrl: string = "";
  getTableDetailsUrl: string = "";
  getStatusListUrl: string = "";
  dmaAllList?: any [];
  dmaStatusList?: any [];
  dmaStatusListRetention?: [];
  sqlArray?: [];
  records?: [];
  userId?: string;
  formData?: any = new FormData();

  exceltoJson: any = {};
  fileObject: any = {
    fileMonth: '',
    fileYear: "",
    procfileMonth: '',
    procfileYear: "",
    userId: "",
    fileTypeId: "",
    fileType: "",
    tableName: "",
    fileName: ""
  };
  selectedFiles?: FileList;
  currentFile?: File;
  isUploadBtnDisabled?: boolean = false;
  isProcessBtnDisabled?: boolean = true;
  isPushBtnDisabled?: boolean = true;
  btnclassblue?: string = "btn btn-blue";
  btnclasssolid?: string = "btn btn-solid";

  uploadbtnclassblue?: string = this.btnclassblue;
  processbtnclasssolid?: string = this.btnclasssolid;
  pushbtnclasssolid?: string = this.btnclasssolid;


  browseButton: boolean = false;
  importButton: boolean = true;
  browseButtonContainer: boolean = false;
  buttonText: string = "Upload";
  buttonTextDropDown: string = "Select a file";
  processButtonContainer: boolean = true;
  pushButtonContainer: boolean = true;

  isButtonUpload: boolean = false;
  isButtonProsess: boolean = false;

  processAPI: string = "";
  importSFDC: string = "";
  importHealthChain: string = "";
  fileUpload: string = "";
  exportSFDC: string = "";

  updatePredictionScore: string = "";



  selectedName: any = {
    fileTypeId: 0,
    fileType: "No Data",
    tablename: "NoData",
    fileName: "NoData",
    Operation: "Upload file"
  };

  sppercent: any;
  progressbarshow = false;
  getSPProgressUrl: string = "";

  constructor(private router: Router, private dataManager: DataManagerService) { 

  }


  dateNavigate($event: NgbDatepickerNavigateEvent) {
    // this.progressbarshow = false;
    console.log("dateNavigate=== ", $event.next.month);
    console.log($event.next.year);
    this.fileObject.fileMonth = $event.next.month;
    this.fileObject.fileYear = $event.next.year;
    this.getStatusList();
    // old value is contained in $event.current
  }
  status1: boolean = true;
  ngOnInit(): void {
    
    this.status1 = !this.status1;

    bsCustomFileInput.init()

    if (sessionStorage.getItem('issprunning') == '1') {
      console.log("CheckProgress caleed", sessionStorage.getItem('processingmonth'));

      this.fileObject.procfileMonth = sessionStorage.getItem('processingmonth');
      this.fileObject.procfileYear = sessionStorage.getItem('processingyear');
      this.CheckProgress();
      this.progressbarshow = true;
      // //this.sppercent=22;
      // this.fileObject.fileMonth = sessionStorage.getItem('processingmonth');
      // this.fileObject.fileYear = sessionStorage.getItem('processingyear');
    }

    this.browseButtonContainer = true;
    this.processButtonContainer = true;
    this.pushButtonContainer = true;
    if (sessionStorage.getItem("token") === null) {
      this.router.navigate(['auth/login']);
    }
    this.successMessage = false;
    this.errorMessage = false;
    var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
    this.userId = currentUser.id

    //Import user uploaded csv files 
    this.updatePredictionScore = this.serverEndPoint + "/rest/private/dma/v1/updatepredictionscore";


    this.uploadUrl = this.serverEndPoint + "/rest/private/dma/v1/user/fileUpload";
    this.getFileTypesUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/type";
    this.getStatusListUrl = this.serverEndPoint + "/rest/private/dma/v1/user/dma/list";
    this.saveUrl = this.serverEndPoint + "/rest/private/dma/v1/user/movefile";
    this.getTableDetailsUrl = this.serverEndPoint + "/rest/private/dma/v1/table/get";
    this.exportSFDC = this.serverEndPoint + "/rest//private/app/v1/exportSFDC";
    //Import csv files from SFDC 
    this.importSFDC = this.serverEndPoint + "/rest/private/app/v1/importSFDC";
    //Import csv files from Healthchain
    this.importHealthChain = this.serverEndPoint + "/rest/private/dma/v1/user/importHealthChain";
    this.processAPI = this.serverEndPoint + "/rest/private/app/v1/process";
    this.getSPProgressUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/SPProgressStatus";
    this.getFileTypeList();


  }

  CheckProgress() {
    this.progressbarshow = true;
    this.sppercent = 5;

    var intervalID = setInterval(() => {
      console.log('getSPStatus ==', this.fileObject.fileMonth, this.fileObject.procfileMonth);
      // this.progressbarshow = true;
      // if (sessionStorage.getItem('issprunning') == '1' && sessionStorage.getItem('processingmonth')==this.fileObject.procfileMonth) {

      this.dataManager.postRequest(this.getSPProgressUrl, this.fileObject)
        .subscribe((response: any) => {
          this.sqlArray = response;
          sessionStorage.setItem('issprunning', '1');
          sessionStorage.setItem('processingmonth', this.fileObject.procfileMonth);
          sessionStorage.setItem('processingyear', this.fileObject.procfileYear);
          console.log("getSPStatus api res", JSON.stringify(response));
          console.log("getSPStatus api length", response.length);
          console.log("issprunning set", sessionStorage.getItem('issprunning'), this.fileObject.fileYear, this.fileObject.fileMonth, this.fileObject.procfileMonth, this.fileObject.procfileYear);
          if (response.length == 5) {
            clearInterval(intervalID);
            sessionStorage.setItem('issprunning', '0');
            //
            let static_result = "";

            this.dataManager.postRequest(this.updatePredictionScore, static_result)
              .subscribe((resultStatus: any) => {
                console.log("Prediction score updated.");
              }, (error: any) => {
                console.log("Prediction api error", error)
              });
            //
            this.ngOnInit();
            this.getStatusList();
          }

          var totalpercent = 0;
          for (var i = 0; response.length > i; i++) {

            console.log("For loop ==== ", response[i].percentage);
            totalpercent = totalpercent + response[i].percentage;
            console.log("totalpercent==== ", totalpercent);
            this.sppercent = totalpercent;

          }
          console.log("sppercent==== ", this.sppercent);


          // clearInterval(intervalID); 

        }, (error: any) => {
          console.log("getSPStatus api error", error)
        });
    }, 5000);

  }

  on_proccess_button_click() {

    this.progressbarshow = true;

    this.fileObject.procfileMonth = this.fileObject.fileMonth;
    this.fileObject.procfileYear = this.fileObject.fileYear;
    //---
    this.CheckProgress();
    //---
    console.log("tablename------ ", this.fileObject);
    this.dataManager.postRequest(this.processAPI, this.fileObject)
      .subscribe((response: any) => {
        this.sqlArray = response;
        console.log("Process api res", response)
      }, (error: any) => {
      });
  }

  on_push_to_salesforce_click() {
    this.isUploadBtnDisabled = false;
    this.isProcessBtnDisabled = true;
    this.isPushBtnDisabled = true;

    this.uploadbtnclassblue = this.btnclassblue;
    this.processbtnclasssolid = this.btnclasssolid;
    this.pushbtnclasssolid = this.btnclasssolid;
    this.dataManager.postRequest(this.exportSFDC, this.fileObject)
      .subscribe((response: any) => {
        this.sqlArray = response;
        console.log("Export api res", response)


      }, (error: any) => {
      });
  }

  selectFile(event: any): void {
    console.log("File Selected------ ", event);
    this.errorMessage = false;
    this.errorMessageText = "";
    this.selectedFiles = event.target.files;
    console.log("File------ ", this.selectedFiles);

    if (this.selectedFiles) {
      this.buttonTextDropDown = event.target.files[0].name
    }
  }

  csvInputChange() {
    console.log("Inside Upload Click...");
    console.log("select type------ ", this.selectedName);
    if (this.selectedName.fileTypeId == 0) {
      this.errorMessage = true;
      this.errorMessageText = "Please Select a File Type.";
      return;
    }

    console.log("select type------ ", this.selectedFiles);
    // this.importButton
    this.fileObject.tableName = "";
    this.fileObject.fileName = "";
    // this.selectedFiles = file.target.files;
    this.fileObject.tableName = this.selectedName.tablename;
    this.fileObject.fileTypeId = this.selectedName.fileTypeId;
    this.fileObject.fileType = this.selectedName.fileType;
    //  this.fileObject.fileName = this.selectedName.fileName;
    this.fileObject.fileName = this.selectedName.tablename + ".csv";
    if (!this.selectedFiles && (this.selectedName.fileTypeId == 2 || this.selectedName.fileTypeId == 3 || this.selectedName.fileTypeId == 4 || this.selectedName.fileTypeId == 10 || this.selectedName.fileTypeId == 12)) {
      console.log("File NOt Selected------ ");
      this.errorMessage = true;
      this.errorMessageText = "Please Select a File to Upload.";
      return;
    } else if (this.selectedName.fileTypeId == 1) {
      console.log("IN If Import Function called..");
      this.import(this.importHealthChain);
    }
    else if (this.selectedName.fileTypeId == 8) {
      console.log("In Else Import Function called..");
      this.import(this.importSFDC);
    } else {
      //this.fileObject.tableName = file[0].tablename;
      this.fileObject.fileName = "";
      console.log("tablename------ ", this.fileObject);
      this.dataManager.postRequest(this.getTableDetailsUrl, this.fileObject)
        .subscribe((response: any) => {
          this.sqlArray = response;
          if (this.selectedFiles) {
            console.log('this.selectedFiles', this.selectedFiles)
            const file: File | null = this.selectedFiles.item(0);
            if (file) {
              this.currentFile = file;
              console.log(this.sqlArray, "array....");
              this.currentFile.name
              // let files = this.currentFile.srcElement.files;
              if (this.currentFile.name.endsWith(".csv")) {
                this.ifCsvFile(this.selectedFiles, file);
              } else if (this.currentFile.name.endsWith(".xlsx")) {
                this.ifXlsxFile(this.selectedFiles, file);
              } else {
                alert("Upload Excel File.");
              }
            }
          }
        }, (error: any) => {
        });
    }
  }

  ifCsvFile(event: any, file: any) {
    // console.log($event.target.files[0], "CSV Files.....");
    let files = event[0];
    let input = event.target;
    let reader = new FileReader();
    reader.readAsText(event[0]);
    reader.onload = () => {
      let csvData = reader.result;
      if (!csvData) {
        this.successMessage = false;
        this.errorMessage = true;
        this.errorMessageText = "Error:" + file.name + " File is Empty...! Please upload the right file.!";
        return;
      }
      let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);
      let fileWithotHeader = csvRecordsArray.length - 1
      if (fileWithotHeader < 1) {
        this.successMessage = false;
        this.errorMessage = true;
        this.errorMessageText = "Error:" + file.name + " File is Empty...! Please upload the right file.! ";
        return;
      }
      let headersRow = this.getHeaderArray(csvRecordsArray);
      console.log("result====================================================", headersRow);
      let result = this.isEqual(headersRow, this.sqlArray, file);
      if (result?.status === "False") {
        this.successMessage = false;
        this.errorMessage = true;
        this.errorMessageText = result?.message;
        return;
      }
      // calling file upload function.
      this.uploadFile(event, file)
    };
    reader.onerror = function () {
      console.log('error is occured while reading file!');
    };
  }

  ifXlsxFile(fileInputEvent: any, file: any) {
    this.exceltoJson = {};
    let headerJson: any = {};
    /* wire up file reader */
    // const target: DataTransfer = <DataTransfer>(fileInputEvent.target);
    // if (target.files.length !== 1) {
    //   throw new Error('Cannot use multiple files');
    // }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(fileInputEvent[0]);
    console.log("filename", fileInputEvent[0].name);
    this.exceltoJson['filename'] = fileInputEvent[0].name;
    reader.onload = (e: any) => {

      let csvData = reader.result;
      console.log(csvData, "file is empty or not");
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      console.log(wb, "is data empty..");
      for (var i = 0; i < wb.SheetNames.length; ++i) {
        const wsname: string = wb.SheetNames[i];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        const data = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
        this.exceltoJson[`sheet${i + 1}`] = data;
        const headers = this.get_header_row(ws);
        headerJson[`header${i + 1}`] = headers;
      }
      this.exceltoJson['headers'] = headerJson;
      var isEmpty = headerJson.header1;

      if (isEmpty.length <= '0') {
        console.log(isEmpty.length, "true");
        this.successMessage = false;
        this.errorMessage = true;
        this.errorMessageText = "Error:" + file.filetype + " File is Empty...! Please upload the right file.! ";
        return;
      }

      let headersRow = this.getHeaderArrayXlsx(headerJson.header1);
      console.log("result====================================================", headersRow);
      var result = this.isEqual(headersRow, this.sqlArray, file);
      if (result?.status === "False") {
        this.successMessage = false;
        this.errorMessage = true;
        this.errorMessageText = result?.message;
        return;
      }
      // Calling Upload Function
      this.uploadFile(fileInputEvent, file)
    };
  }

  // Upload Function.
  uploadFile(fileInputEvent: any, file: any) {
    console.log("uploadFile====== ", fileInputEvent[0].name);
    this.errorMessage = false;
    this.successMessage = false;
    this.formData.delete('fileType')
    this.formData.delete('upload')
    this.formData.delete('userId')
    this.formData.delete('fileTypeId')
    this.formData.delete('fileDateYear')
    this.formData.delete('fileMonth')
    this.formData.delete('tableName')
    this.formData.delete('fileYear')
    this.formData.delete('fileName')
    this.formData.append("fileMonth", this.fileObject.fileMonth);
    this.formData.append("fileYear", this.fileObject.fileYear);
    this.formData.append("fileTypeId", this.fileObject.fileTypeId);
    this.formData.append("fileType", this.fileObject.fileType);
    this.formData.append("tableName", this.fileObject.tableName);
    this.formData.append("userId", this.userId);
    this.formData.append("upload", fileInputEvent[0]);
    this.formData.append("fileName", fileInputEvent[0].name);

    console.log(this.fileObject, "object data..");
    this.errorMessage = false;
    this.successMessage = false;
    this.waitMessage = true;
    this.waitMessageText = "Please Wait.. File Is Uploading..";
    this.dataManager.postRequest(this.uploadUrl, this.formData)
      .subscribe((response: any) => {
        this.ngOnInit();
        this.getStatusList();
        this.waitMessage = false;
        this.errorMessage = false;
        this.successMessage = true;
        this.successMessageText = "File Uploaded Successfully.!";
        this.router.navigate(['dashboard/file']);
      }, (error: any) => {
        if (error.status == 401) {
        } else {
        }
      });
  }

  isEqual(a: any, b: any, file: any) {
    console.log("Calling isEqual Function..", a.length, "-", b.length);
    if (a.length != b.length) {
      return { 'status': "False", 'message': "Error: In a " + file.name + " File fields are missing.!  " + JSON.stringify(b) };
    } else {
      var successArray = [];
      for (var i = 0; i < b.length; i++) {
        console.log(a[i].COLUMN_NAME, b[i].COLUMN_NAME);
        if (a[i].COLUMN_NAME == b[i].COLUMN_NAME && a[i].ORDINAL_POSITION == b[i].ORDINAL_POSITION) {
          successArray.push(b[i].COLUMN_NAME);
          if (successArray.length === b.length) {
            return { 'status': "True", 'message': "Success" };
          }
        } else {
          return { 'status': "False", 'message': "Error: In a " + file.name + " file  : [ " + b[i].COLUMN_NAME + " ] field is missing on column postion number : [ " + b[i].ORDINAL_POSITION + " ]. Please Correct & Upload Right File.!" };
        }
      }
    }
  }

  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];
    let i = 1;
    for (let j = 0; j < headers.length; j++) {
      headerArray.push({ "COLUMN_NAME": headers[j].replace(/"/g, ""), "ORDINAL_POSITION": i });
      i++;
    }
    return headerArray;
  }

  getHeaderArrayXlsx(csvRecordsArr: any) {
    let headerArray = [];
    let i = 1;
    for (let j = 0; j < csvRecordsArr.length; j++) {
      headerArray.push({ "COLUMN_NAME": csvRecordsArr[j].replace(/"/g, ""), "ORDINAL_POSITION": i });
      i++;
    }
    return headerArray;
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let csvArr = [];
    for (let i = 1; i < csvRecordsArray.length; i++) {
      let curruntRecord = (<string>csvRecordsArray[i]).split(',');
      if (curruntRecord.length == headerLength) {
        // let csvRecord: CSVRecord = new CSVRecord();  
        // csvRecord.id = curruntRecord[0].trim();  
        // csvRecord.firstName = curruntRecord[1].trim();  
        // csvRecord.lastName = curruntRecord[2].trim();  
        // csvRecord.age = curruntRecord[3].trim();  
        // csvRecord.position = curruntRecord[4].trim();  
        // csvRecord.mobile = curruntRecord[5].trim();  
        // csvArr.push(csvRecord);  
      }
    }
    // return csvArr;  
  }

  getFileTypeList() {
    console.log("inside getFileTypeList");
    this.fileObject.userId = this.userId
    this.dataManager.postRequest(this.getFileTypesUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "GetFileTypes List Response1111111111......");
        // this.dmaAllList = response;

        this.dmaAllList = [
          // {
          //   "id": 1,
          //   "filetype": "Statewide Demographics Data",
          //   "tablename": "axiom",
          //   "Operation": "Import from HealthChain"
          // },
          {
            "id": 2,
            "filetype": "Do Not Call",
            "tablename": "do_not_call",
            "Operation": "Upload file"
          },
          {
            "id": 3,
            "filetype": "Enterprise Data Warehouse",
            "tablename": "edr_p65_combine_data",
            "Operation": "Upload file"
          },
          // {
          //   "id": 4,
          //   "filetype": "Website",
          //   "tablename": "prodcopy",
          //   "Operation": "Upload file"
          // },
          {
            "id": 8,
            "filetype": "Salesforce Support",
            "tablename": "sdfc_leads_source_hist",
            "Operation": "Import from Salesforce"
          },
          {
            "id": 10,
            "filetype": "Marketing Data",
            "tablename": "marketo",
            "Operation": "Upload file"
          },
          // {
          //   "id": 12,
          //   "filetype": "SFDC Sales Source Hist",
          //   "tablename": "sdfc_sales_source_hist",
          //   "Operation": "Upload file"
          // }
        ]
 
        // this.router.navigate(['file']);
      }, (error: any) => {
      });
  }

  getStatusList() {
    console.log("Get List DMA Status getStatusListUrl", this.fileObject);
    this.fileObject.userId = this.userId
    this.dataManager.postRequest(this.getStatusListUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "get status List....");
        this.dmaStatusListRetention = response;
        // this.dmaStatusList = response;
        this.dmaStatusList  = (this.addFileType(this.dmaStatusListRetention));
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log(error, "error get status List....");
        if (error.status == 404) {
          this.dmaStatusList = [];
        }
      });
  }

  addFileType(temp: any) {
    console.log(temp, "get temp List....");
    var tempArray =[]
    for (var i = 0; i < temp.length; i++) {
      if (temp[i].filetype === "Do Not Call") {
        tempArray.push(temp[i]);
      }
      if (temp[i].filetype === "Enterprise Data Warehouse") {
        tempArray.push(temp[i]);
      }
      if (temp[i].filetype === "Salesforce Leads") {
        tempArray.push(temp[i]);
      } 
      if (temp[i].filetype === "Marketing Data") {
        tempArray.push(temp[i]);
      } 
    }
    return tempArray;
  }

 
  // changefiletype(e: { target: { value: number; }; }) {
  changefiletype(e: any) {
    console.log(this.selectedName, "selectedName");
    this.errorMessage = false;
    this.errorMessageText = "";
    this.successMessage = false;
    if (this.selectedName.fileTypeId == 2 || this.selectedName.fileTypeId == 3 || this.selectedName.fileTypeId == 4 || this.selectedName.fileTypeId == 10 || this.selectedName.fileTypeId == 12) {
      this.browseButtonContainer = true;
      // this.buttonTextDropDown = "Select a file";
      this.buttonText = this.selectedName.Operation
    } else {
      this.browseButtonContainer = false;
      this.buttonText = this.selectedName.Operation

    }
    // this.fileObject.filetype = e.target.value;
    // console.log("Resulted File Object ", JSON.stringify(this.fileObject));
  }


  onchangeMonth(changeMonth: any) {
    console.log("month", changeMonth);
    this.errorMessage = false;
    this.successMessage = false;
    this.fileObject.fileMonth = changeMonth;
    //this.getFileTypeList();
  }



  onchangeYear(changeYear: any) {
    this.errorMessage = false;
    this.successMessage = false;
    this.fileObject.fileYear = changeYear
    //this.getFileTypeList();
  }

  saveFile() {
    if (!this.fileObject.fileMonth) {
      this.errorMessage = true;
      this.errorMessageText = "Please Select a Month."
      return;
    }
    if (!this.fileObject.fileYear) {
      this.errorMessage = true;
      this.errorMessageText = "Please Select a Year."
      return;
    }
    var status = (this.checkIfExist(this.dmaAllList));
    if (status.status) {
      this.successMessage = false;
      this.errorMessage = true;
      this.errorMessageText = "Info : Please Upload All Files..!"
      return;
    }
    this.dataManager.postRequest(this.saveUrl, { "userId": this.userId, "month": this.fileObject.fileMonth, "year": this.fileObject.fileYear })
      .subscribe((response: any) => {
        console.log(response, "responce file save responce....");
        this.successMessage = true;
        this.errorMessage = false;
        this.successMessageText = response.message
        this.getFileTypeList();
        // this.getStatusList();
        // this.router.navigate(['dashboard/file']);
      }, (error: any) => {
        if (error.status == 400) {
          this.successMessage = false;
          this.errorMessage = true;
          this.errorMessageText = error.error.message
        }
      });
  }

  checkIfExist(dmaList: any) {
    var found = {
      'status': false
    };
    for (var i = 0; i < dmaList.length; i++) {
      if (dmaList[i].isUploaded === null) {
        found = {
          'status': true
        };
      }
    }
    return found;
  }

  import(APIURL: any) {
    console.log("Import url..", APIURL);
    this.errorMessage = false;
    this.successMessage = false;
    this.waitMessage = true;
    this.waitMessageText = "Please Wait... Leads Data Importing!";
    // this.isSubmitBtnDisabled = true;
    this.isButtonUpload = true;
    this.isButtonProsess = true;
    // this.importExportObject.userId = "1";
    this.dataManager.postRequest(APIURL, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "Import SFDC or HealthChain responce");
        this.ngOnInit();
        this.getStatusList();
        this.waitMessage = false;
        this.errorMessage = false;
        this.successMessage = true;
        this.successMessageText = "Leads Data Imported Successfully.!";
        this.isButtonUpload = false;
        this.isButtonProsess = false;
        // this.isSubmitBtnDisabled = false;
        // this.router.navigate(['dashboard/file']);
        // this.ngOnInit();
        // this.router.navigate(['dashboard/export-import']);
      }, (error: any) => {
        this.successMessage = false;
        this.waitMessage = false;
        this.errorMessage = true;
        this.isButtonUpload = true;
        this.isButtonProsess = true;
        this.errorMessageText = "Leads Data Import Failed..!";
        // this.isSubmitBtnDisabled = false;
      });
  }

  get_header_row(sheet: any) {
    var fileLength = Object.keys(sheet).length;
    if (fileLength > 0) {
      var headers = [];
      var range = XLSX.utils.decode_range(sheet['!ref']);
      var C, R = range.s.r; /* start in the first row */
      /* walk every column in the range */
      for (C = range.s.c; C <= range.e.c; ++C) {
        var cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })] /* find the cell in the first row */
        // console.log("cell",cell)
        var hdr = "UNKNOWN " + C; // <-- replace with your desired default 
        if (cell && cell.t) {
          hdr = XLSX.utils.format_cell(cell);
          headers.push(hdr);
        }
      }
      return headers;
    } else {
      return [];
    }
  }


  pre() {


  }

  next() {

  }
}
