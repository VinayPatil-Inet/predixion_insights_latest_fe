//added for select box
import { FormBuilder, FormGroup } from '@angular/forms';
import { AfterViewInit, EventEmitter, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { DataManagerService } from '../../services/data-manager.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { NgbDatepickerNavigateEvent } from '@ng-bootstrap/ng-bootstrap';
import { Options } from '@angular-slider/ngx-slider';
const APIEndpoint = environment.APIEndpoint;
class Person {
  id?: number;
  firstName?: string;
  lastName?: string;
}
class DataTablesResponse {
  data?: any[];
  draw?: number;
  recordsFiltered?: number;
  recordsTotal?: number;
}
@Component({
  selector: 'sb-predictions',
  templateUrl: './predictions.component.html',
  styleUrls: ['./predictions.component.scss']
})
export class PredictionsComponent implements AfterViewInit, OnDestroy, OnInit {

  @ViewChild(DataTableDirective, { static: false })
  dtElement?: DataTableDirective;

  errorMessage: boolean = false;
  successMessage: boolean = false;
  errorMessageText?: string;
  successMessageText?: string;
  waitMessage: boolean = false;
  waitMessageText?: string;
  isSubmitBtnDisabled: boolean = false;
  paginationCount?: number;

  dtTrigger: Subject<any> = new Subject();
  public serverEndPoint = APIEndpoint;
  getIntTableUrl: string = "";
  totalPushToSalesForce: string = "";
  userId: string = "";

  minScore: string = '0t25';
  minValue: number = 16;
  maxValue: number = 100;
  options: Options = {
    floor: 1,
    ceil: 120
  };

  currentYear: number = new Date().getFullYear();

  fileObject: any = {
    fileMonth: new Date().getMonth() + 1,
    fileYear: new Date().getFullYear(),
    state: [],
    city: [],
    income: [],
    zipcode: [],
    age: { min: 16, max: 120 },
    integratedLead: [],
    isChecked: false,
    isCheckedLeadsAll: false,
    // score: '0t25'
    score: [{ id: '75t100', value: 'Extremely High', ischecked: false }, { id: '50t75', value: 'High', ischecked: false }, { id: '25t50', value: 'Medium', ischecked: false }, { id: '0t25', value: 'Below Medium', ischecked: false }],
  };

  // Dropdown 
  myForm?: FormGroup;
  disabled = false;
  ShowFilter = true;
  limitSelection = false;
  selectedItems?: any[];
  dropdownSettings: any = {};

  getCityListUrl: string = "";
  getStateListUrl: string = "";
  getZipcodeListUrl: string = "";
  getIncomeListUrl: string = "";
  getAgeListUrl: string = "";
  pushToSalesForceUrl: string = "";
  checkboxSelectAllUrl: string = "";
  updatePrediction: string = "";
  predictionUrl: string = "";
  updatePredictionScore: string = "";
  totalLeadCount?: string = '0';
  cities?: any[];
  stats?: any[];
  zipcodes?: any[];
  income?: any[];

  dtOptions: DataTables.Settings = {};
  persons?: Person[];
  public constructor(private router: Router, private dataManager: DataManagerService, private activatedRoute: ActivatedRoute, private http: HttpClient, private fb: FormBuilder) {
    this.activatedRoute.params.subscribe(params => {
      if (params.month != undefined) {
        this.fileObject.fileMonth = params.month;
        this.fileObject.fileYear = params.year;
        console.log("URL Month ", this.fileObject);
      }
    })
  }

  public ngOnInit(): void {
    var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
    this.userId = currentUser.id
    //API urls
    this.getCityListUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/city";
    this.getStateListUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/state";
    this.getZipcodeListUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/zipcode";
    this.getIncomeListUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/income";
    this.getAgeListUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/age";
    this.pushToSalesForceUrl = this.serverEndPoint + "/rest/private/dma/v1/insertsaleforcedata";
    this.checkboxSelectAllUrl = this.serverEndPoint + "/rest/private/dma/v1/user/files/selectAllRow";
    this.updatePrediction = this.serverEndPoint + "/rest/private/dma/v1/updateprediction";
    this.predictionUrl = "http://164.52.194.25:8017/predict-sale";
    this.updatePredictionScore = this.serverEndPoint + "/rest/private/dma/v1/updatepredictionscore";


    this.fileObject.state = [];
    this.fileObject.city = [];
    this.fileObject.income = [];
    this.fileObject.zipcode = [];

    //call api
    this.getCityList();
    this.getStateList();
    this.getZipcodeList();
    this.getIncomeList();
    // this.selectedItems = [{ item_id: 4, item_text: 'Pune' }, { item_id: 6, item_text: 'Navsari' }];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: this.ShowFilter
    };

    if (sessionStorage.getItem("token") === null) {
      this.router.navigate(['auth/login']);
    }
    console.log("Calling init function...");
    this.getIntTableUrl = this.serverEndPoint + "/rest/private/dma/v1/table/pagination";
    console.log("API URl...", this.getIntTableUrl);
    const that = this;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      responsive: true,
      dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"i><"col-sm-2"l><"col-sm-6"p>>',
      searching: true,
      language: {
        /* paginate: {
            previous: "<i class='fa fa-angle-left arrow left' aria-hidden='true'></i>",
            next: "<i class='fa fa-angle-right arrow right' aria-hidden='true'></i>"
          }*/
      },
    };
    this.dtOptions = {
      //  Type : { previous: String, next: String },
      pagingType: 'full_numbers',
      language: {
        // paginate: {
        //   previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
        //   next: "<i class='fa fa-angle-right' aria-hidden='true'></i>"
        // }
      },
      pageLength: 10,
      scrollY: '400',
      scrollX: true,
      serverSide: true,
      processing: true,
      // dom: '<"top"i>rt<"bottom"flp><"clear">',
      dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"i><"col-sm-2"l><"col-sm-6"p>>',
      searching: false,
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters['dateObject'] = this.fileObject;
        that.http
          .post<DataTablesResponse>(
            this.getIntTableUrl,
            dataTablesParameters, {}
          ).subscribe(resp => {
            console.log(resp, "responce data......");
            var updatedResponse = (this.selectRow(resp.data));
            // console.log(updatedResponse, "testing row................................................................................. data......");
            // that.persons = resp.data;
            that.persons = updatedResponse;
            var totalcount = resp.recordsTotal;
            this.paginationCount =totalcount; 
            console.log(totalcount, "pagination total row.........................."); 
            // if (totalcount) {
            //   if (totalcount > 0) {
            //     console.log(totalcount, "totalcount  true....................................................");
            //     this.isSubmitBtnDisabled = false;
            //     this.totalPushToSalesForce = "Push " + totalcount + " Selected To SalesForce";
            //   }
            // } else {
            //   console.log(totalcount, "totalcount  flase....................................................");
            //   this.isSubmitBtnDisabled = true;
            //   this.totalPushToSalesForce = "No leads Selected To Push";
            // }
            if (typeof this.fileObject.integratedLead != "undefined" && this.fileObject.integratedLead != null && this.fileObject.integratedLead.length != null && this.fileObject.integratedLead.length > 0) {
              // console.log(this.fileObject.integratedLead.length, "totalcount  true....................................................");
              this.isSubmitBtnDisabled = false;
              this.totalPushToSalesForce = "Push " + this.fileObject.integratedLead.length + " Selected To Salesforce";
            } else {
              // console.log(totalcount, "totalcount  flase....................................................");
              this.isSubmitBtnDisabled = true;
              this.totalPushToSalesForce = "No leads Selected To Push";
            }

            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },
      columns: [{ data: 'isSelected' }, { data: 'integreted_id' }, { data: 'MAD_ID' },
      { data: 'first_name' },
      { data: 'middle_initial' },
      { data: 'age' },
      { data: 'address_line1' },
      { data: 'city' },
      { data: 'state' },
      { data: 'zip_code' },
      { data: 'income' },
      { data: 'phone' },
      { data: 'prediction_score' },
      { data: 'member_email' },
      { data: 'MAD_Data_Source' }
      ]
    };
  }



  pushToSalesForce() {
    this.errorMessage = false;
    this.successMessage = false;
    this.waitMessage = true;
    this.waitMessageText = "Please Wait.. Leads Are Exporting Into Salesforce..!";
    this.isSubmitBtnDisabled = true;
    this.fileObject.userId = this.userId;
    console.log("inside pushToSalesForce", this.fileObject);
    this.dataManager.postRequest(this.pushToSalesForceUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(" pushToSalesForce response", response);
        this.ngOnInit();
        this.waitMessage = false;
        this.errorMessage = false;
        this.successMessage = true;
        this.successMessageText = "Leads Are Exported Successfully Into Salesforce..!";
        this.isSubmitBtnDisabled = false;
        console.log(response, "getIncomeList response");
        this.income = response;
      }, (error: any) => {
        this.successMessage = false;
        this.waitMessage = false;
        this.errorMessage = true;
        this.errorMessageText = "Leads Export Failed..!";
        this.isSubmitBtnDisabled = false;
      });
  }
  // Update Prediction.
  updatePredictionFun() {
    console.log("Inside Update Prediction function..");
    this.errorMessage = false;
    this.successMessage = false;
    this.waitMessage = true;
    this.waitMessageText = "Please Wait.. Lead Scores Are Updating From Prediction..!";
    this.isSubmitBtnDisabled = true;
    this.fileObject.userId = this.userId;
    console.log("Prediction Payload", this.fileObject);
    this.dataManager.postRequest(this.updatePrediction, this.fileObject)
      .subscribe((PredictionResponse: any) => {
        console.log("Prediction response", PredictionResponse);
        // this.dataManager.postRequest(this.predictionUrl, PredictionResponse)
        //   .subscribe((result: any) => {
        // console.log("Prediction score response", result);
        let static_result = [
          {
            "sale_probability": 43.78,
            "unique_id": 17
          },
          {
            "sale_probability": 27.86,
            "unique_id": 27
          },
          {
            "sale_probability": 99.68,
            "unique_id": 47
          }
        ]
        this.dataManager.postRequest(this.updatePredictionScore, static_result)
          .subscribe((resultStatus: any) => {
            console.log("Prediction score updated.", resultStatus);
          }, (error: any) => {
            console.log("Prediction api error", error)
          });
        // }, (error: any) => {
        //   console.log("Prediction api error", error)
        // });

        this.ngOnInit();
        this.waitMessage = false;
        this.errorMessage = false;
        this.successMessage = true;
        this.successMessageText = "Leads Score Successfully Updated from Prediction..!";
        this.isSubmitBtnDisabled = false;
        // this.income = response;
      }, (error: any) => {
        this.successMessage = false;
        this.waitMessage = false;
        this.errorMessage = true;
        this.errorMessageText = "Prediction Update Failed..!";
        this.isSubmitBtnDisabled = false;
      });
  }

  rangeChangeMin(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    this.fileObject.age.min = item.target.value;
    console.log('Range change ,......', item.target.value);
    this.rerender();

  }
  rangeChangeMax(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    this.fileObject.age.max = item.target.value;
    console.log('Range change ,......', item.target.value);
    this.rerender();

  }

  changeScore(event: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('score change ,......', event.target.value);
    this.fileObject.score = event.target.value;
    console.log('object', this.fileObject);
    this.rerender();
  }

  //Dropdown City----------
  onItemSelect(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemSelect', item);
    this.fileObject.city.push(item.item_text);
    this.getStateList();
    this.getZipcodeList();
    this.rerender();
    console.log('fileObject push', this.fileObject);
  }

  onSelectAll(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onSelectAll', items);
    this.fileObject.city = [];
    for (var i = 0; i < items.length; i++) {
      this.fileObject.city.push(items[i].item_text);
    }
    this.getStateList();
    this.getZipcodeList();
    this.rerender();
    console.log('fileObject push All City', this.fileObject.city);
  }

  onItemDeSelectAll(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelectAll', items);
    this.fileObject.city = [];
    this.getStateList();
    this.getZipcodeList();
    this.rerender()
    console.log('fileObject POP All City', this.fileObject.city);
  }

  onItemDeSelect(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelect', item);
    for (var i = 0; i < this.fileObject.city.length; i++) {
      if (this.fileObject.city[i] === item.item_text) {
        console.log(item.item_text, "true", this.fileObject);
        this.fileObject.city.splice(i, 1);
      }
    }
    // this.fileObject.city.pop(item.item_text);
    this.getStateList();
    this.getZipcodeList();
    this.rerender();
    console.log('fileObject pop', this.fileObject.city);
  }

  toogleShowFilter() {
    this.ShowFilter = !this.ShowFilter;
    this.dropdownSettings = Object.assign({}, this.dropdownSettings, { allowSearchFilter: this.ShowFilter });
  }

  handleLimitSelection() {
    if (this.limitSelection) {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: 2 });
    } else {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: null });
    }
  }

  getCityList() {
    console.log("inside getCityList", this.fileObject);
    this.dataManager.postRequest(this.getCityListUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "getCityList response");
        this.cities = response;
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log(error, "getCityList error");
        this.cities = [];
      });
  }
  // City End--------------

  // State dropdown------
  onItemSelectState(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemSelect', item);
    this.fileObject.state.push(item.item_text);
    this.getCityList();
    this.getZipcodeList();
    this.rerender();
    console.log('fileObject push', this.fileObject.state);
  }

  onSelectAllState(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onSelectAll', items);
    this.fileObject.state = [];
    for (var i = 0; i < items.length; i++) {
      this.fileObject.state.push(items[i].item_text);
    }
    this.getCityList();
    this.getZipcodeList();
    this.rerender();
    console.log('fileObject push All state', this.fileObject.state);
  }

  onItemDeSelectAllState(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelectAll', items);
    this.fileObject.state = [];
    this.getCityList();
    this.getZipcodeList();
    this.rerender();
    console.log('fileObject POP All state', this.fileObject.state);
  }

  onItemDeSelectState(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelect', item);
    for (var i = 0; i < this.fileObject.state.length; i++) {
      if (this.fileObject.state[i] === item.item_text) {
        console.log(item.item_text, "true", this.fileObject);
        this.fileObject.state.splice(i, 1);
      }
    }
    // this.fileObject.state.pop(item.item_text);
    this.getCityList();
    this.getZipcodeList();
    this.rerender();
    console.log('fileObject pop', this.fileObject.state);
  }

  getStateList() {
    console.log("inside getStateList", this.fileObject);
    this.dataManager.postRequest(this.getStateListUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "getStateList response");
        this.stats = response;
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log(error, "getStateList error");
        this.stats = [];
      });
  }
  // State End-------------------

  // Zipcode dropdown------------
  onItemSelectZipcode(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemSelect', item);
    this.fileObject.zipcode.push(item.item_text);
    this.getStateList();
    this.getCityList();
    this.rerender();
    console.log('fileObject push', this.fileObject.zipcode);
  }

  onSelectAllZipcode(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onSelectAll', items);
    this.fileObject.zipcode = [];
    for (var i = 0; i < items.length; i++) {
      this.fileObject.zipcode.push(items[i].item_text);
    }
    this.getStateList();
    this.getCityList();
    this.rerender();
    console.log('fileObject push All zipcode', this.fileObject.zipcode);
  }

  onItemDeSelectAllZipcode(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelectAll', items);
    this.fileObject.zipcode = [];
    this.getStateList();
    this.getCityList();
    this.rerender();
    console.log('fileObject POP All zipcode', this.fileObject.zipcode);
  }

  onItemDeSelectZipcode(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelect', item);
    for (var i = 0; i < this.fileObject.zipcode.length; i++) {
      if (this.fileObject.zipcode[i] === item.item_text) {
        console.log(item.item_text, "true", this.fileObject);
        this.fileObject.zipcode.splice(i, 1);
      }
    }
    // this.fileObject.zipcode.pop(item.item_text);
    this.getStateList();
    this.getCityList();
    this.rerender();
    console.log('fileObject pop', this.fileObject.zipcode);
  }

  getZipcodeList() {
    console.log("inside getZipcodeList", this.fileObject);
    this.dataManager.postRequest(this.getZipcodeListUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "getZipcodeList response");
        this.zipcodes = response;
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log(error, "getZipcodeList error");
        this.zipcodes = [];
      });
  }
  // Zipcode End------------

  // Income dropdown---------
  onItemSelectIncome(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemSelectIncome', item);
    this.fileObject.income.push(item.item_text);
    this.rerender();
    console.log('fileObject push', this.fileObject.income);
  }

  onSelectAllIncome(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onSelectAllIncome', items);
    this.fileObject.income = [];
    for (var i = 0; i < items.length; i++) {
      this.fileObject.income.push(items[i].item_text);
    }
    this.rerender();
    console.log('fileObject push All income', this.fileObject.income);
  }

  onItemDeSelectAllIncome(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelectAllIncome', items);
    this.fileObject.income = [];
    console.log('fileObject POP All income', this.fileObject.income);
    this.rerender();
  }

  onItemDeSelectIncome(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelectIncome', item);
    for (var i = 0; i < this.fileObject.income.length; i++) {
      if (this.fileObject.income[i] === item.item_text) {
        console.log(item.item_text, "true", this.fileObject);
        this.fileObject.income.splice(i, 1);
      }
    }
    // this.fileObject.income.pop(item.item_text);
    this.rerender();
    console.log('fileObject pop ', this.fileObject.income);
  }

  getIncomeList() {
    console.log("inside getIncomeList", this.fileObject);
    this.dataManager.postRequest(this.getIncomeListUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "getIncomeList response");
        this.income = response;
        // this.router.navigate(['file']);
      }, (error: any) => {
        console.log(error, "getIncomeList error");
        this.income = [];
      });
  }
  // Income End---------------

  // Age dropdown--------------
  onItemSelectAge(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemSelectAge', item);
    this.fileObject.age.push(item.item_text);
    this.rerender();
    console.log('fileObject push', this.fileObject.age);
  }

  onSelectAllAge(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onSelectAllAge', items);
    this.fileObject.age = [];
    for (var i = 0; i < items.length; i++) {
      this.fileObject.age.push(items[i].item_text);
    }
    this.rerender();
    console.log('fileObject push All age', this.fileObject.age);
  }

  onItemDeSelectAllAge(items: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelectAllAge', items);
    this.fileObject.age = [];
    console.log('fileObject POP All age', this.fileObject.age);
  }

  onItemDeSelectAge(item: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log('onItemDeSelectAge', item);

    for (var i = 0; i < this.fileObject.age.length; i++) {
      if (this.fileObject.age[i] === item.item_text) {
        console.log(item.item_text, "true", this.fileObject);
        this.fileObject.age.splice(i, 1);
      }
    }
    // this.fileObject.age.pop(item.item_text);
    this.rerender();
    console.log('fileObject pop ', this.fileObject.age);
  }

  getAgeList() {
    console.log("inside getAgeList", this.fileObject);
    this.dataManager.postRequest(this.getAgeListUrl, this.fileObject)
      .subscribe((response: any) => {
        console.log(response, "getAgeList response");
        // this.age = response;
        // this.router.navigate(['file']);
      }, (error: any) => {

      });
  }
  // Age End-----------------

  public isNewLead(value: boolean) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    if (value) {
      this.fileObject.isChecked = value;
      console.log("true", this.fileObject);
      this.rerender();
    } else {
      this.fileObject.isChecked = value;
      console.log("false", this.fileObject);
      this.rerender();
    }
  }

  public isExtreme(value: boolean, id: any) {
    this.fileObject.isCheckedLeadsAll =false;
    this.fileObject.integratedLead =[];
    console.log("value", value, id);
    if (value) {
      for (var i = 0; i < this.fileObject.score.length; i++) {
        if (this.fileObject.score[i].id == id + "") {
          this.fileObject.score[i].ischecked = value;
        }
      }
      console.log("true", this.fileObject);
      this.rerender();

    } else {
      for (var i = 0; i < this.fileObject.score.length; i++) {
        if (this.fileObject.score[i].id == id + "") {
          this.fileObject.score[i].ischecked = value;
        }
      }
      console.log("false", this.fileObject);
      this.rerender();
    }
  }

  public isChekedLeadRow(value: boolean, id: any) {
    console.log(id, "id..................................................................................................");
    if (value) {
      this.fileObject.integratedLead.push(id);
      console.log("true", this.fileObject);
      this.rerender();
    } else {
      for (var i = 0; i < this.fileObject.integratedLead.length; i++) {
        if (this.fileObject.integratedLead[i] === id) {
          console.log(id, "true", this.fileObject);
          this.fileObject.integratedLead.splice(i, 1);
        }
      }
      console.log("false", this.fileObject);
      this.fileObject.isCheckedLeadsAll = false;
      this.rerender();
    }
  }

  selectRow(data: any) {
    // console.log("inside selectRow............................", this.fileObject.integratedLead);
    if (this.fileObject.isCheckedLeadsAll) {
      for (var j = 0; j < data.length; j++) {
        if (typeof this.fileObject.integratedLead != "undefined" && this.fileObject.integratedLead != null && this.fileObject.integratedLead.length != null && this.fileObject.integratedLead.length > 0) {
          for (var i = 0; i < this.fileObject.integratedLead.length; i++) {
            // console.log("lenght............................", this.fileObject.integratedLead[i], data[j].integreted_id);
            if (this.fileObject.integratedLead[i] + "" == data[j].integreted_id + "") {
              data[j].isSelected = true;
              if (i === this.fileObject.integratedLead.length) {
                break;
              }
            }
          }
        } else {
          this.fileObject.integratedLead.push(data[j].integreted_id);
          data[j].isSelected = true;
        }
      }
      // console.log("lenght............................", this.fileObject.integratedLead.lenght);
    } else {
      // for (var j = 0; j < data.length; j++) {
      //   data[j].isSelected = false;
      // }
      // this.fileObject.integratedLead = []
      // this.fileObject.isCheckedLeadsAll = false;
      // console.log("false", this.fileObject);

      for (var j = 0; j < data.length; j++) {
        if (typeof this.fileObject.integratedLead != "undefined" && this.fileObject.integratedLead != null && this.fileObject.integratedLead.length != null && this.fileObject.integratedLead.length > 0) {
          for (var i = 0; i < this.fileObject.integratedLead.length; i++) {
            console.log("lenght............................", this.fileObject.integratedLead[i], data[j].integreted_id);
            if (this.fileObject.integratedLead[i] + "" == data[j].integreted_id + "") {
              data[j].isSelected = true;
              if (i === this.fileObject.integratedLead.length) {
                break;
              }
            }
          }
        } else {
          for (var j = 0; j < data.length; j++) {
            data[j].isSelected = false;
          }
          this.fileObject.integratedLead = []
          this.fileObject.isCheckedLeadsAll = false;
          console.log("false", this.fileObject);
        }
      }
    }
    //--

    //--
    return data;
  }


  isChekedLeadRowAll(value: boolean) {
    console.log("this.fileObject", this.fileObject);
    if (value) {
      this.fileObject.isCheckedLeadsAll = true;
      this.fileObject.integratedLead = [];
      this.dataManager.postRequest(this.checkboxSelectAllUrl, this.fileObject)
        .subscribe((response: any) => {
          for (var j = 0; j < response.data.length; j++) {
            this.fileObject.integratedLead.push(response.data[j].integreted_id);

          }
          this.rerender();
        }, (error: any) => {
        });
      // this.rerender();
    } else {
      this.fileObject.integratedLead = []
      this.fileObject.isCheckedLeadsAll = false;
      console.log("false", this.fileObject);
      this.rerender();
    }
  }

  // Save file --------------
  saveFile() {
    if (!this.fileObject.fileMonth) {
      this.errorMessage = true;
      this.errorMessageText = "Please Select a Month."
      return;
    }
    if (!this.fileObject.fileYear) {
      this.errorMessage = true;
      this.errorMessageText = "Please Select a Year."
      return;
    }
    console.log("Saving object...", this.fileObject);
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  onchangeMonth(changeMonth: any) {
    console.log("month", changeMonth);
    // this.errorMessage = false;
    // this.successMessage = false;
    this.fileObject.fileMonth = changeMonth;
  }

  onchangeYear(changeYear: any) {
    // this.errorMessage = false;
    // this.successMessage = false;
    this.fileObject.fileYear = changeYear
  }

  rerender(): void {
    console.log("rendering file......");
    this.dtElement?.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();

      // Call the dtTrigger to rerender again
      this.dtTrigger.next();

    });
  }

  dateNavigate($event: NgbDatepickerNavigateEvent) {
    console.log("Inside data Nevigate");
    console.log($event.next.month, "month");
    console.log($event.next.year, "year");
    this.fileObject.fileMonth = $event.next.month;
    this.fileObject.fileYear = $event.next.year;
    this.fileObject.state = [];
    this.fileObject.city = [];
    this.fileObject.income = [];
    this.fileObject.zipcode =[];
    this.fileObject.age = { min: 16, max: 120 },
    this.fileObject.integratedLead =[];
    this.fileObject.isChecked = false;
    this.fileObject.isCheckedLeadsAll =false;
    this.getStateList();
    this.getCityList();
    this.getZipcodeList();
    this.rerender();
  }

}
