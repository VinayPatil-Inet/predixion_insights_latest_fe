import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionIntViewComponent } from './retention-int-view.component';

describe('MemberretentionComponent', () => {
  let component: RetentionIntViewComponent;
  let fixture: ComponentFixture<RetentionIntViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetentionIntViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionIntViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
