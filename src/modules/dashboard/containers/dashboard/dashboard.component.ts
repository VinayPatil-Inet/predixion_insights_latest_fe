import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DataManagerService } from '../../services/data-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
const APIEndpoint = environment.APIEndpoint;

@Component({
    selector: 'sb-dashboard',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './dashboard.component.html',
    styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  public serverEndPoint = APIEndpoint;

    fileObject: any = {
        fileMonth: 8,
        fileYear: "2020",
        userId: "",
        filetype: "",
        tableName: ""
      };

      selectedDisplayMonth: string = "August";

      monthObject: any = {
        January: "bg-secondary",
        February: "bg-secondary",
        March: "bg-secondary",
        April: "bg-secondary",
        May: "bg-secondary",
        June: "bg-secondary",
        July: "bg-primary",
        August: "bg-secondary",
        September: "bg-secondary",
        October: "bg-secondary",
        November: "bg-secondary",
        December: "bg-secondary",
     
      };
      errorMessage: boolean = false;
      successMessage: boolean = false;
      userId?: string;
      getDmaListUrl: string = "";
      dmaAllList?: [];
     
    constructor(private router: Router, private dataManager: DataManagerService) {}


    ngOnInit():void {

 if (sessionStorage.getItem("token") === null) {
     // this.router.navigate(['auth/login']);
      //...
    }
        console.log("dashboared new is loading");
        this.successMessage = false;
        this.errorMessage = false;
        var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
        this.userId = currentUser.id
        this.getDmaListUrl = this.serverEndPoint+"/rest/private/dma/v1/user/dma/list/";
        this.getDmaList();
    }

    getDmaList() {
        // console.log("Get List DMA");
        this.fileObject.userId = this.userId
        this.dataManager.postRequest(this.getDmaListUrl, this.fileObject)
          .subscribe((response: any) => {
            console.log(response, "DMA List responce");
            this.dmaAllList = response;
            // this.router.navigate(['file']);
          }, (error: any) => {
          });
      }

    onchangeYear(changeYear: any) {
        console.log("Calling Year Function....");
        this.errorMessage = false;
        this.successMessage = false;
        this.fileObject.fileYear = changeYear
        this.getDmaList();
      }

      onchangeMonth(changeMonth: any, data: any) {
        console.log("Calling Month Function....", changeMonth, data);
        this.clear();
        this.monthObject[changeMonth] = "bg-primary"
        console.log(changeMonth, "selected..");
        this.errorMessage = false;
        this.successMessage = false;
        this.fileObject.fileMonth = data;
        this.selectedDisplayMonth = changeMonth;
        this.getDmaList();
      }

      clear(){
        this.monthObject.January =  "bg-secondary",
        this.monthObject.February =  "bg-secondary",
        this.monthObject.March =  "bg-secondary",
        this.monthObject.April =  "bg-secondary",
        this.monthObject.May =  "bg-secondary",
        this.monthObject.June =  "bg-secondary",
        this.monthObject.July =  "bg-secondary",
        this.monthObject.August =  "bg-secondary",
        this.monthObject.September =  "bg-secondary",
        this.monthObject.October =  "bg-secondary",
        this.monthObject.November =  "bg-secondary", 
        this.monthObject.December =  "bg-secondary"
      }

      viewIntTable(){
        console.log("VIew Integrated table", this.fileObject);
        this.router.navigate(['/dashboard/file-int-view/'+this.fileObject.fileMonth+'/'+this.fileObject.fileYear]);
        // this.router.navigate(['file-int-view'], {month:   this.fileObject.fileMonth  });
      }
}
