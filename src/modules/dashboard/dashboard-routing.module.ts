/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { DashboardModule } from './dashboard.module';

/* Containers */
import * as dashboardContainers from './containers';

/* Guards */
import * as dashboardGuards from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.DashboardnewComponent,
    },
    {
        path: 'file',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.FileuploadComponent,
    },

    {
        path: 'dashboardnew',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.DashboardnewComponent,
    },

    
    {
        path: 'reports',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.ReportsComponent,
    },

    {
        path: 'file-int-view',
        // path: 'file-int-view/:month/:year',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.ViewGridComponent,
    },

    
    {
        path: 'predictions',
        // path: 'file-int-view/:month/:year',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.PredictionsComponent,
    },
    
    {
        path: 'memberdetails/:filter',
        // path: 'file-int-view/:month/:year',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.MemberdetailsComponent,
    },
    
    
    {
        path: 'memberretention',
        // path: 'file-int-view/:month/:year',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.MemberretentionComponent,
    },

    {
        path: 'rerentionfileupload',
        // path: 'file-int-view/:month/:year',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.RerentionFileuploadComponent,
    },

    {
        path: 'retention-int-view',
        // path: 'file-int-view/:month/:year',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.RetentionIntViewComponent,
    },

    {
        path: 'retention-dashboard',
        // path: 'file-int-view/:month/:year',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.RetentionDashboardComponent,
    },
    
    {
        path: 'export-import',
        // path: 'file-int-view/:month/:year',
        data: {
            title: 'Dashboard - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.ExportImportComponent,
    },
    {
        path: 'static',
        data: {
            title: 'Dashboard Static - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/dashboard',
                },
                {
                    text: 'Static',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.StaticComponent,
    },
    {
        path: 'light',
        data: {
            title: 'Dashboard Light - BCBSIR',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/dashboard',
                },
                {
                    text: 'Light',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.LightComponent,
    },
];

@NgModule({
    imports: [DashboardModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class DashboardRoutingModule {}
