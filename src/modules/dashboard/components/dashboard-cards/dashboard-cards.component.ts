import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DataManagerService } from '../../services/data-manager.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'sb-dashboard-cards',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './dashboard-cards.component.html',
    styleUrls: ['dashboard-cards.component.scss'],
})
export class DashboardCardsComponent implements OnInit {

    fileObject: any = {
        fileMonth: "July",
        fileYear: "2020",
        userId: "",
        filetype: "",
        tableName: ""
      };
      errorMessage: boolean = false;
      successMessage: boolean = false;
      userId?: string;
      getDmaListUrl: string = "";
      dmaAllList?: [];


    constructor(private router: Router, private dataManager: DataManagerService) {}


    ngOnInit() {

        console.log("dashboared component is loading");
        this.successMessage = false;
        this.errorMessage = false;
        var currentUser = JSON.parse(sessionStorage.getItem('user') || '{}');
        this.userId = currentUser.id
        this.getDmaListUrl = "http://localhost:7001/rest/private/dma/v1/user/dma/list/";
        this.getDmaList();
    }

    getDmaList() {
        // console.log("Get List DMA");
        this.fileObject.userId = this.userId
        this.dataManager.postRequest(this.getDmaListUrl, this.fileObject)
          .subscribe((response: any) => {
            console.log(response, "DMA List responce");
            this.dmaAllList = response;
            // this.router.navigate(['file']);
          }, (error: any) => {
          });
      }

    onchangeYear(changeYear: any) {
        console.log("Calling Year Function....");
        this.errorMessage = false;
        this.successMessage = false;
        this.fileObject.fileYear = changeYear
        this.getDmaList();
      }

      onchangeMonth(changeMonth: any) {
        console.log("Calling Month Function....", changeMonth);
        this.errorMessage = false;
        this.successMessage = false;
        this.fileObject.fileMonth = changeMonth;
        this.getDmaList();
      }
}
