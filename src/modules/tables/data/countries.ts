import { Country } from '../models';

export const COUNTRIES: Country[] = [
    {
        id: 1,
        name: 'Acxion',
        flag: 'f/f3/Flag_of_Russia.svg',
        area: 31231,
        population: 146989754,
    },
    {
        id: 2,
        name: 'Cinical',
        flag: 'c/c3/Flag_of_France.svg',
        area: 640679,
        population: 64979548,
    },
    {
        id: 3,
        name: 'Do Not Copy',
        flag: 'b/ba/Flag_of_Germany.svg',
        area: 357114,
        population: 82114224,
    },
    {
        id: 4,
        name: 'EDR',
        flag: '5/5c/Flag_of_Portugal.svg',
        area: 92090,
        population: 10329506,
    },
    {
        id: 5,
        name: 'HOST',
        flag: 'c/cf/Flag_of_Canada.svg',
        area: 9976140,
        population: 36624199,
    },
    {
        id: 6,
        name: 'National Aliance',
        flag: '2/21/Flag_of_Vietnam.svg',
        area: 331212,
        population: 95540800,
    },
    {
        id: 7,
        name: 'ProdCopy',
        flag: '0/05/Flag_of_Brazil.svg',
        area: 8515767,
        population: 209288278,
    },
    {
        id: 8,
        name: 'SPDC',
        flag: 'f/fc/Flag_of_Mexico.svg',
        area: 1964375,
        population: 129163276,
    },
  
];
